//
//  BaseNetwork.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

protocol BaseNetwork {

    func processResponse<T: Codable>(response: URLResponse?,
                                     data: Data?,
                                     error: Error?,
                                     decodable: T.Type,
                                     completion: @escaping (T?, Error?) -> Void)
}

extension BaseNetwork {

    func processResponse<T: Codable>(response: URLResponse?,
                                     data: Data?,
                                     error: Error?,
                                     decodable: T.Type,
                                     completion: @escaping (T?, Error?) -> Void) {

        self.verifyRequestResponse(response, error: error) { result in
            switch result {
            case .success:
                guard let data = data else {
                    completion(nil, NetworkError.unknownError)
                    return
                }

                do {
                    let jsonData = try JSONDecoder().decode(T.self, from: data)
                    completion(jsonData, nil)
                } catch {
                    completion(nil, error)
                }
            case .failure(let error):
                guard let _ = data else {
                    completion(nil, error)
                    return
                }
                completion(nil, error)
            }
        }
    }

    private func verifyRequestResponse(_ response: URLResponse?, error: Error?, completion: (Result<Bool, Error>) -> Void) {
        guard error == nil else {
            completion(.failure(error!))
            return
        }

        guard let statusCode = response?.httpStatusCode else {
            // shouldn't happen but let's be safe
            completion(.failure(NetworkError.unknownError))
            return
        }
        switch statusCode {
        case 200...210:
            completion(.success(true))
        case 400...404:
            completion(.failure(NetworkError.badRequest(url: response?.url)))
        case 500...599:
            completion(.failure(NetworkError.serverNotResponding(status: statusCode)))
        default:
            completion(.failure(NetworkError.unknownError))
        }
    }
}

extension Data {
    var prettyJson: String? {
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = String(data: data, encoding: .utf8) else { return nil }

        return prettyPrintedString
    }
}
