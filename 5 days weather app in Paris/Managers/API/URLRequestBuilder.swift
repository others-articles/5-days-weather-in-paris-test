//
//  URLRequestBuilder.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

struct URLRequestBuilder<Target: Endpoint> {

    /**
     Create an URL request for a givent EndPoint
     
     - Parameter target: The endpoint with all the information to build the request
     - Throws: `NetworkError.requestBuildSetParamsFailed`
     if the builder doesn't succed to create a request with parameters
     - Returns: A new URLRequest
     */
    mutating func buildRequest(target: Target, timeoutInterval: TimeInterval) throws -> URLRequest {
        var request: URLRequest

        switch target.task {
        case .empty:
            request = try buildParamsRequest(target: target, timeoutInterval: timeoutInterval, with: nil)
        case .plain:
            request = try buildParamsRequest(target: target, timeoutInterval: timeoutInterval, with: [:])
        case .withParams(let params):
            request = try buildParamsRequest(target: target, timeoutInterval: timeoutInterval, with: params)
        }
        return request
    }

    // MARK: - Builder

    /**
     Create an URL request with no parameters
     
     - Returns: A new URLRequest
     
     */
    private mutating func buildPlainRequest(target: Target, timeoutInterval: TimeInterval) -> URLRequest {
        var request = URLRequest(url: target.baseURL.appendingPathComponent(target.path),
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: timeoutInterval)
        request.httpMethod = target.httpMethod.rawValue
        return request
    }

    /**
     Create an URL request with a given parameters. For a GET request those parameters NEED to be convertible into a string.
     
     - Parameter target: The endpoint with all the information to build the request
     - Parameter params: A dictionnay that contains all the parameters to set in the URLRequest
     - Throws: `NetworkError.requestBuildSetParamsFailed`
     if the builder doesn't succed to create a request with parameters
     - Returns: A new URLRequest
     */
    private mutating func buildParamsRequest(target: Target, timeoutInterval: TimeInterval, with params: [String: Any]?) throws -> URLRequest {
        switch target.httpMethod {
        case .GET:
            let request = try buildURLComponent(target: target, with: params)
            return request
        case .POST, .PUT, .PATCH, .DELETE:
            let request = buildPlainRequest(target: target, timeoutInterval: timeoutInterval)
            return try setRequestParams(request, params: params)
        }
    }

    /**
     This function creates firstly URLComponent because to build a URLRequest with parameters such as a query params, an URLComponents is necessary to add the parameters in the URL, then it creates an URLRequest and returns it
     
     - Parameter target: The endpoint with all the information to build the request
     - Parameter params: A dictionnay that contains all the parameters to set in the URLComponent. Those parameters NEED to be convertible into a string
     - Throws: `NetworkError.requestBuildSetParamsFailed`
     if the builder doesn't succed to create a request with parameters
     - Returns: A new URLRequest
     */
    private func buildURLComponent(target: Target, with params: [String: Any]?) throws -> URLRequest {
        guard let url = URL(string: target.baseURL.absoluteString + target.path) else {
            throw NetworkError.requestBuildSetParamsFailed
        }
        var components = URLComponents(string: url.absoluteString)

        guard components != nil else {
            throw NetworkError.requestBuildSetParamsFailed
        }

        if let params = params {
            if !params.isEmpty {
                components!.queryItems = [URLQueryItem]()
                params.forEach {
                    components!.queryItems!.append(URLQueryItem(name: $0.key, value: "\($0.value)"))
                }
                // Apple acknowledges that we have to percent escaping the + characters, but advises that we do it manually:
                components!.percentEncodedQuery = components!.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
            }
        }
        var request = URLRequest(url: components!.url!)
        request.httpMethod = target.httpMethod.rawValue
        return request
    }

    // MARK: - Convenients
    
    /**
     This function set the parameters for a given request.
     
     - Parameter request: The request that need the parameters to be added
     - Parameter params: A dictionnay that contains all the parameters to set in the URLRequest
     - Throws: `NetworkError.requestBuildSetParamsFailed`
     if the builder doesn't succed to create a request with parameters
     - Returns: A new URLRequest
     */
    private mutating func setRequestParams(_ request: URLRequest, params: [String: Any]?) throws -> URLRequest {
        if let params = params {
            var pRequest = request
            do {
                pRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                return pRequest
            } catch {
                throw NetworkError.requestBuildSetParamsFailed
            }
        }
        return request
    }
}

struct AnyEncodable: Encodable {

    let encodable: Encodable

    init(_ encodable: Encodable) {
        self.encodable = encodable
    }

    func encode(to encoder: Encoder) throws {
        try encodable.encode(to: encoder)
    }
}
