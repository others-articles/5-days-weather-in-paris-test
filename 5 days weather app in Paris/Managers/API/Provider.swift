//
//  Provider.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

class Provider<Target: Endpoint>: NSObject, URLSessionDelegate {
    
    private var requestBuilder = URLRequestBuilder<Target>()
    private var session: URLSession!
    private var dispatchQueue: DispatchQueue
    
    init(dispatchQueue: DispatchQueue? = nil) {
        if dispatchQueue != nil {
            self.dispatchQueue = dispatchQueue!
        } else {
            self.dispatchQueue = DispatchQueue.global(qos: .userInitiated)
        }
        super.init()
        self.session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
    }
    
    func request<T: Codable>(_ target: Target, decodable: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
        do {
            let request = try requestBuilder.buildRequest(target: target, timeoutInterval: TimeInterval(Constants.shared.timeInterval))
            self.execute(request) { [weak self] (response, data, responseError) in
                self?.processResponse(response: response, data: data,
                                      error: responseError,
                                      decodable: decodable) { (data, error) in

                    guard error == nil else {
                        completion(.failure(error!))
                        return
                    }
                    
                    guard let jsonData = data else {
                        // response data is nil, could be a parse issue
                        completion(.failure(NetworkError.unknownError))
                        return
                    }

                    completion(.success(jsonData))
                }
            }
        } catch {
            completion(.failure(error))
        }
    }
    
    /*  Download file, if you need to dl a file you may use  let data = Data(contentsOf: URL) in your code to avoid to implement a new api call for it. */
    func execute(_ request: URLRequest, completionHandler: ((URLResponse?, Data?, Error?) -> Void)? = nil) {
        dispatchQueue.async(qos: .userInitiated) {
            let task = self.session.dataTask(with: request) { data, response, error in
                DispatchQueue.main.async {
                    completionHandler!(response, data, error)
                }
            }
            task.resume()
        }
    }
    
}

extension Provider: BaseNetwork { }
