//
//  ModuleManager.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 21/03/2023.
//

import Foundation

class DependencyContainer {
    private var modules: [String: Any] = [:]

    func register<T>(_ module: T) {
        modules[String(describing: T.self)] = module
    }

    func resolve<T>() -> T {
        guard let module = modules[String(describing: T.self)] as? T else {
            fatalError("Module not registered: \(T.self)")
        }
        return module
    }
}


class ModuleManager {
    
    static let shared = ModuleManager()
    
    private let container = DependencyContainer()

    private init() {
        registerModules()
    }

    private func registerModules() {
        container.register(CoreDataModuleContainer.CoreDataModule())
    }

    func getCoreDataModule() -> CoreDataModule {
        return container.resolve()
    }
}
