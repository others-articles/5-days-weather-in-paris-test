//
//  DetailedViewController.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import UIKit


class DetailedViewController: BaseViewController {
    
    private var viewModel: DetailedViewModel!
    
    //Mark: UI Elements
    
    private lazy var minTempLabel = {
        let label = UILabel()
        label.font = Fonts.shared.mainTempFont
        label.textColor = UITheme.shared.primary
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var maxTempLabel = {
        let label = UILabel()
        label.font = Fonts.shared.mainTempFont
        label.textColor = UITheme.shared.primary
        label.textAlignment = .right
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var infoLabel = {
        let label = UILabel()
        label.font = Fonts.shared.labelFont
        label.textColor = UITheme.shared.black
        label.textAlignment = .right
        label.numberOfLines = 0
        return label
    }()
    
    override func setupViews() {
        super.setupViews()
        view.addSubview(minTempLabel)
        view.addSubview(maxTempLabel)
        view.addSubview(infoLabel)
    }
    
    override func setupLayouts() {
        super.setupLayouts()
        minTempLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(UISpace.shared.minimalSpacing)
            make.height.equalTo(UISize.shared.mainTempSize)
            make.right.equalToSuperview()
            make.left.equalToSuperview().inset(UISpace.shared.minimalSpacing)
            make.centerX.equalToSuperview()
        }
        
        maxTempLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(UISpace.shared.minimalSpacing)
            make.height.equalTo(UISize.shared.mainTempSize)
            make.left.equalToSuperview()
            make.right.equalToSuperview().inset(UISpace.shared.minimalSpacing)
            make.centerX.equalToSuperview()
        }
        
        infoLabel.snp.makeConstraints { make in
            make.top.equalTo(minTempLabel.snp.bottom).offset(UISpace.shared.verticalSpacing)
            make.height.equalTo(UISize.shared.label)
            make.centerX.equalToSuperview()
        }
    }
    
    //Mark: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        showData()
    }
    
    //Mark: functions
    
    private func showData() {
        minTempLabel.text = viewModel.getMinTemp()
        maxTempLabel.text = viewModel.getmaxTemp()
    }
    
    func bind(vm: DetailedViewModel) {
        self.viewModel = vm
    }
    
    override func localize() {
        super.localize()
        infoLabel.text = "DetailedViewController.view.label.info".localized()
    }
}
