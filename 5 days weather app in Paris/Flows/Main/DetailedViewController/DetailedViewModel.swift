//
//  DetailedViewModel.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation


class DetailedViewModel {
    
    private var data: Daily
    
    init(dayData: Daily) {
        self.data = dayData
    }
    
    func getMinTemp() -> String {
        let formatter = TemperatureFormatter()
        if let formatted = formatter.formatTemperature(temp: data.temp.min) {
            return formatted
        }
        return ""
    }
    
    func getmaxTemp() -> String {
        let formatter = TemperatureFormatter()
        if let formatted = formatter.formatTemperature(temp: data.temp.max) {
            return formatted
        }
        return ""
    }
}
