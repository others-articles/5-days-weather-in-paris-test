//
//  ViewController.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 19/03/2023.
//

import UIKit

class MainViewController: BaseViewController {
    
    // MARK: Variables
    
    private var viewModel: MainViewModel!
    
    // Mark: UI Elements
    
    private lazy var loadingLabel = {
        let label = UILabel()
        label.font = Fonts.shared.labelFont
        label.textAlignment = .center
        label.textColor = UITheme.shared.black
        return label
    }()
    
    private lazy var tableView: UITableView = { // TodaysTempsTVCell
        let tableView = UITableView()
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
        tableView.backgroundColor = .clear
        tableView.register(TodaysTempsTVCell.self, forCellReuseIdentifier: Constants.TVCellID.todaysTemps.rawValue)
        tableView.register(DaysWeatherTVCell.self, forCellReuseIdentifier: Constants.TVCellID.daysWeather.rawValue)
        return tableView
    }()
    
    override func setupViews() {
        super.setupViews()
        self.view.addSubview(tableView)
        self.view.addSubview(loadingLabel)
    }
    
    override func setupLayouts() {
        super.setupLayouts()
        
        loadingLabel.snp.makeConstraints { make in
            make.height.equalTo(UISize.shared.label)
            make.center.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    //Mark: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = MainViewModel(delegate: self)
        viewModel.getData()
    }
    
    //Mark: Functions
    
    override func localize() {
        super.localize()
        loadingLabel.text = "MainViewController.view.label.loading".localized()
    }
}

extension MainViewController: MainViewModelDelegate {
    func didNotGetData() {
        loadingLabel.isHidden = false
        loadingLabel.text = "MainViewController.view.label.noData".localized()
    }
    
    func didPressDayRow(at: Int) {
        if let data = viewModel.getDayData(at: at) {
            (coordinator as! MainCoordinator).presentDetailedView(data: data)
        }
    }
    
    func didGetData() {
        loadingLabel.isHidden = true
        viewModel.setTableViewData()
        viewModel.configCells(tv: tableView)
        tableView.delegate = viewModel
        tableView.dataSource = viewModel
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

