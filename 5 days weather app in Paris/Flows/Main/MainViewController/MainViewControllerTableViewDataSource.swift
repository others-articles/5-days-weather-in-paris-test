//
//  MainViewControllerTableViewDataSource.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import UIKit

protocol MainViewTableViewDataSourceProtocol: NSObject {
    func didPressRow(at: Int)
}


class MainViewControllerTableViewDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private weak var delegate: MainViewTableViewDataSourceProtocol?
    private var rows = [UITableViewCell]()
    private var data: OpenWeatherResponse?
    
    init(delegate: MainViewTableViewDataSourceProtocol) {
        self.delegate = delegate
    }
    
    func setData(data: OpenWeatherResponse) {
        self.data = data
    }
    
    func configCells(tv: UITableView) {
        rows.append(configTempCell(tv: tv))
        for i in 0...4 {
            rows.append(configDayCell(tv: tv, i: i))
        }
    }
    
    func configTempCell(tv: UITableView) -> UITableViewCell {
        let formatter = TemperatureFormatter()
        guard let data = data else { return UITableViewCell() }
        guard let cell = tv.dequeueReusableCell(withIdentifier: Constants.TVCellID.todaysTemps.rawValue) as? TodaysTempsTVCell else {
            return TodaysTempsTVCell()
        }
        if let formattedDayTemp = formatter.formatTemperature(temp: data.current.temp) {
            cell.setupAttributes(temp: formattedDayTemp)
        }
        return cell
    }
    
    func configDayCell(tv: UITableView, i: Int) -> UITableViewCell {
        let formatter = TemperatureFormatter()
        guard let data = data else { return UITableViewCell() }
        guard let cell = tv.dequeueReusableCell(withIdentifier: Constants.TVCellID.daysWeather.rawValue) as? DaysWeatherTVCell else {
            return DaysWeatherTVCell()
        }
        let dayTemp = data.daily[i].temp.day
        if let nameDay = getDateLabelatIndex(i: i), let formattedDayTemp = formatter.formatTemperature(temp: dayTemp) {
            cell.setupAttributes(temp: formattedDayTemp, dayName: nameDay)
        }
        return cell
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = rows[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didPressRow(at: indexPath.row)
    }
}

extension MainViewControllerTableViewDataSource {
    func getDateLabelatIndex(i: Int) -> String? {
        let formatter = DayFormatter()
        if let data = data {
            let date = formatter.getDateFromTimeStamp(dt: (data.daily[i].dt))
            return formatter.getNameOfDay(date: date)
        }
        return nil
    }
}
