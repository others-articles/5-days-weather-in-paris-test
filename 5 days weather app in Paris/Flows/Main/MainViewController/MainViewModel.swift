//
//  MainViewModel.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation
import UIKit

protocol MainViewModelDelegate: NSObjectProtocol {
    func didGetData()
    func didNotGetData()
    func didPressDayRow(at: Int)
}

class MainViewModel: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    private weak var delegate: MainViewModelDelegate?
    private var weatherData: OpenWeatherResponse?
    private var weatherService: WeatherDataClientProtocol
    private var geoService: GeoLatLonClientProtocol
    private var coreDataManager: CoreDataModule
    private var networkUtil: NetworkUtil
    private lazy var tableViewDelegate: MainViewControllerTableViewDataSource =  {
        let datasource = MainViewControllerTableViewDataSource(delegate: self)
        return datasource
    }()
    private var geoSearchLimit = "1"
    private let city = "Paris"
    private var geoData: GeoResponse?
    
    init(delegate: MainViewModelDelegate, weatherService: WeatherDataClientProtocol = WeatherDataClient(), geoService: GeoLatLonClientProtocol = GeoLatLonClient()) {
        self.delegate = delegate
        self.weatherService = weatherService
        self.geoService = geoService
        self.coreDataManager = ModuleManager.shared.getCoreDataModule()
        self.networkUtil = NetworkUtil()
    }
    
    func getData() {
        if networkUtil.connectedToNetwork() {
            getGeoData()
        } else {
            coreDataManager.getManagedObject(forEntityName: "WeatherDataEntity") { [weak self] error, context, object in
                if let object = object,
                    let data = object.value(forKey: "weatherData") as? OpenWeatherResponse {
                    self?.weatherData = data
                } else {
                    self?.delegate?.didNotGetData()
                }
            }
        }
    }
    
    func getDayData(at: Int) -> Daily? {
        if let data = weatherData {
            return data.daily[at]
        }
        return nil
    }
    
    private func getGeoData() {
        geoService.retrieve(city: city, limit: geoSearchLimit, apiKey: Constants.shared.OpenWeatherAPIKey, decodable: GeoResponse.self) { [weak self] response, error in
            guard let response = response else {
                self?.delegate?.didNotGetData()
                return
            }
            self?.geoData = response
            if let city = response.first {
                self?.getWeatherData(lat: "\(city.lat)", long: "\(city.lon)")
            }
        }
    }
    
    private func getWeatherData(lat: String, long: String) {
        weatherService.retrieve(lat: lat, lon: long, apiKey: Constants.shared.OpenWeatherAPIKey, metric: Constants.shared.metric, decodable: OpenWeatherResponse.self) { [weak self] response, error in
            guard let response = response else {
                self?.delegate?.didNotGetData()
                return
            }
            self?.weatherData = response
            self?.saveData()
            self?.delegate?.didGetData()
        }
    }
    
    func saveData() { // TODO not functionnal yet.. 
        do {
            let result = try coreDataManager.fetch(entityName: "WeatherDataEntity")
            let objects = result.1
            let context = result.0
            if objects.indices.contains(1),
                let object = objects.first {
                self.coreDataManager.addValueToManagedObject(managedObject: object, key: "weatherData", value: self.weatherData as Any)
                self.coreDataManager.saveManagedObject(context: context)
            } else if let managedObject = self.coreDataManager.createNewMAnagedObject(entityName: "WeatherDataEntity", context: context) {
                self.coreDataManager.addValueToManagedObject(managedObject: managedObject, key: "weatherData", value: self.weatherData as Any)
                self.coreDataManager.saveManagedObject(context: managedObject.managedObjectContext!)
            }
        } catch _ {
          print("Couldn't save data")
        }
    }
    
    //Mark: UITableviewDelegate & DataSource
    
    func setTableViewData() {
        if let data = weatherData {
            tableViewDelegate.setData(data: data)
        }
    }
    
    func configCells(tv: UITableView) {
        tableViewDelegate.configCells(tv: tv)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableViewDelegate.tableView(tableView, cellForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewDelegate.tableView(tableView, numberOfRowsInSection: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewDelegate.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewDelegate.tableView(tableView, heightForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didPressRow(at: indexPath.row)
    }
}

extension MainViewModel: MainViewTableViewDataSourceProtocol {
    func didPressRow(at: Int) {
        delegate?.didPressDayRow(at: at)
    }
}
