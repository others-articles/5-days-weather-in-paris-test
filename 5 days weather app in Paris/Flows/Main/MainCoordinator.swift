//
//  MainCoordinator.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import UIKit

class MainCoordinator: CoordinatorProtocol {
    
    var navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func shouldPush(vc: UIViewController, animated: Bool = true) {
        navigationController.pushViewController(vc, animated: animated)
    }
    
    func shouldPop(animated: Bool = true) {
        navigationController.popViewController(animated: animated)
    }
    
    func startFlow() -> UINavigationController {
        let vc = MainViewController(coordinator: self)
        navigationController.viewControllers = [vc]
        return navigationController
    }
    
    func startNewFlow() { }
    
    func presentDetailedView(data: Daily) {
        let vm = DetailedViewModel(dayData: data)
        let vc = DetailedViewController(coordinator: self)
        vc.bind(vm: vm)
        navigationController.pushViewController(vc, animated: true)
    }
}
