//
//  BaseViewController.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import UIKit
import SnapKit

class BaseViewController: UIViewController {
    
    // Mark: variables
    var coordinator: CoordinatorProtocol!
    
    override var preferredStatusBarStyle : UIStatusBarStyle { return UIStatusBarStyle.lightContent }
    
    // Mark: UI Elements
    
    private lazy var backgroundImageView = {
        let imageView = UIImageView(image: UIImage(named: "background"))
        return imageView
    }()
    
    private lazy var navBarImageView = {
        let imageView = UIImageView(image: UIImage(named: "veepee_logo_text"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    // MARK: Lifecycle
    
    init(coordinator: CoordinatorProtocol) {
        super.init(nibName: nil, bundle: nil)
        self.coordinator = coordinator
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupLayouts()
        localize()
    }
    
    // MARK: Private functions
    
    private  func setupNavBarTheme(navBar: UINavigationBar) {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UITheme.shared.white
        navBar.standardAppearance = appearance
        navBar.scrollEdgeAppearance = navBar.standardAppearance
    }
    
    // MARK: Functions
    
    func setupViews() {
        if let navigationController = navigationController {
            setupNavBarTheme(navBar: navigationController.navigationBar)
            showNavBar()
        }
        view.addSubview(backgroundImageView)
        let backBarBtnItem = UIBarButtonItem()
        backBarBtnItem.tintColor = UITheme.shared.primary
        navigationController?.navigationBar.backItem?.backBarButtonItem = backBarBtnItem
        navigationItem.titleView = navBarImageView
    }
    
    func setupLayouts() {
            backgroundImageView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
    }
    
    func localize() { }
    
    func hideNavBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func showNavBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

