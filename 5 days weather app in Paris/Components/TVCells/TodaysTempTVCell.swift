//
//  TodaysTempTVCell.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import UIKit
import SnapKit

class TodaysTempsTVCell: UITableViewCell {
    
    //Mark: UI Element
    
    private lazy var todayLabel = {
        let label = UILabel()
        label.font = Fonts.shared.labelFont
        label.textColor = UITheme.shared.black
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = "today".localized()
        return label
    }()
 
    private lazy var tempLabel = {
        let label = UILabel()
        label.font = Fonts.shared.mainTempFont
        label.textColor = UITheme.shared.primary
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    func setupViews() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        contentView.addSubview(tempLabel)
        contentView.addSubview(todayLabel)
    }
    
    func setupLayout() {
        
        todayLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(UISpace.shared.smallSpacing)
            make.left.equalToSuperview().offset(UISpace.shared.smallSpacing)
            make.right.equalToSuperview()
            make.height.equalTo(UISize.shared.label)
        }
        
        tempLabel.snp.makeConstraints { make in
            make.top.equalTo(todayLabel.snp.bottom).offset(UISpace.shared.minimalSpacing)
            make.height.equalTo(UISize.shared.mainTempSize)
            make.left.right.equalToSuperview()
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    //Mark: Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .white
        setupViews()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Mark: funcs
    
    func setupAttributes(temp: String) {
        tempLabel.text = temp
    }
 }
 
