//
//  Request.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

enum Request {
    case plain
    case empty
    case withParams(params: [String: Any])
}
