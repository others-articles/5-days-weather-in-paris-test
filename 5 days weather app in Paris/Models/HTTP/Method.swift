//
//  Method.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

struct Method {
    private init() {
    }

    enum Http: String {
        case GET
        case POST
        case PUT
        case PATCH
        case DELETE
    }
}
