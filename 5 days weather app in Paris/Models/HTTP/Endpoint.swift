//
//  Endpoint.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

protocol Endpoint {
    var path: String {get}
    var httpMethod: Method.Http {get}
    var task: Request {get}
    var baseURL: URL {get}
}


extension Endpoint {
    var httpMethod: Method.Http {
        return .GET
    }

    /*
    var baseURL: URL? {
        if let url = URL(string: Constants.shared.requestBase) {
            return url
        }
        return nil
    }
     */
}
