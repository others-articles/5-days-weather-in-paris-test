//
//  NetworkError.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

enum NetworkError: Error {
    case unknownError
    case noConnectivity
    case parseError
    case badRequest(url: URL?)
    case serverNotResponding(status: Int)
    case requestBuildFailed
    case requestBuildSetParamsFailed
}

extension NetworkError: LocalizedError, CustomDebugStringConvertible {

    var errorDescription: String? { return description }
    var failureReason: String? { return description }
    var debugDescription: String { return description }

    private var description: String {
        switch self {
        case .serverNotResponding(let status):
            return "Server error: \(status)"
        case .badRequest(let url):
            return "Bad request: \(String(describing: url))"
        default:
            return "NetworkError - unknown"
        }
    }
}

