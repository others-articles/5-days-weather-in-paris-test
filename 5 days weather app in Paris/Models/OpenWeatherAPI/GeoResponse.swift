//
//  GeoResponse.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 21/03/2023.
//

import Foundation

typealias GeoResponse = [City]

struct City: Codable {
    let name: String
    let lat, lon: Double
    let country, state: String

    enum CodingKeys: String, CodingKey {
        case name
        case lat, lon, country, state
    }
}
