//
//  Minutely.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

struct Minutely: Codable {
    let dt, precipitation: Int
}
