//
//  Weather.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

struct Weather: Codable {
    let id: Int
    let main: MainWeather
    let description: Description
}
