//
//  Temps.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

struct Temp: Codable {
    let day, min, max, night: Double
    let eve, morn: Double
}
