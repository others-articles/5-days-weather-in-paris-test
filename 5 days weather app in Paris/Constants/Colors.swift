//
//  Colors.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 19/03/2023.
//

import UIKit

class UITheme {
    
    static let shared: UITheme = UITheme()
    
    private init() {}
    
    let primary = UIColor(hex: "EC008C")
    
    let secondary = UIColor(hex: "575762")
    
    let black = UIColor(hex: "000000")
    
    let white = UIColor(hex: "FFFFFF")
}

