//
//  Spaces.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

class UISpace {
    
    static let shared: UISpace = UISpace()
    
    private init() {}
    
    let minimalSpacing = 4
    
    let tightSpacing = 8
    
    let smallSpacing = 16
    
    let mediumSpacing = 24
    
    let commonSpacing = 32
    
    let verticalSpacing = 32

}
