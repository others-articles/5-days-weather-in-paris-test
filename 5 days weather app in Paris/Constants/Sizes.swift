//
//  Sizes.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

class UISize {
    
    static let shared: UISize = UISize()
    
    private init() {}
    
    let mainTempSize = 60
    
    let titleLabel = 30
    
    let label = 24
}

