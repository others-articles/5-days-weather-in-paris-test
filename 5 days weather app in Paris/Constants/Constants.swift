//
//  Constants.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

class Constants {
    
    static let shared: Constants = Constants()
    
    private init() {}
    
    // Mark: HTTP
    let timeInterval = 60
    
    // MARK: Open Weather
    let OpenWeatherAPIKey = "b03a5cce927ba64313ac0a9bce90bc62"
    let requestBase = "https://api.openweathermap.org"
    let parisLat = "48.8588897"
    let parisLon = "2.3200410217200766"
    let metric = "metric"
    
    // TV cells
    
    enum TVCellID: String {
        case todaysTemps = "TodaysTempsTVCell"
        case daysWeather = "DaysWeatherTVCell"
    }
    
    // Core Data
    let entityName = "WeatherDataEntity"
    
}
