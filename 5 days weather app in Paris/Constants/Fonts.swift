//
//  Fonts.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

import UIKit

class Fonts {
    
    static let shared: Fonts = Fonts()
    
    private init() {}
    
    let mainTempFont = UIFont.boldSystemFont(ofSize: CGFloat(54))
    
    let titleFont = UIFont.boldSystemFont(ofSize: CGFloat(24))
    
    let subtitleFont = UIFont.boldSystemFont(ofSize: CGFloat(16))
    
    let labelFont = UIFont.systemFont(ofSize: 18)
    
    let boldLabelFont = UIFont.boldSystemFont(ofSize: CGFloat(18))
}
