//
//  DayFormatter.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 21/03/2023.
//

import Foundation

class DayFormatter {
    
    func getDateFromTimeStamp(dt: Int) -> NSDate {
            return NSDate(timeIntervalSince1970: Double(dt))
    }
    
    func getNameOfDay(date: NSDate) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: date as Date)
    }
}
