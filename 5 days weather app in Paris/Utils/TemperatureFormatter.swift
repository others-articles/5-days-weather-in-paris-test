//
//  TemperatureFormatter.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 21/03/2023.
//

import Foundation

class TemperatureFormatter {
    
    func formatTemperature(temp: String) -> String? {
        if let _ = Double(temp) {
            let parts = temp.components(separatedBy: ".")
            if let integerPart = parts.first {
                return "\(integerPart)" + "°"
            }
        }
        return nil
    }
    
    func formatTemperature(temp: Double) -> String? {
        let stringValue = String(format: "%.2f", temp)
        let parts = stringValue.components(separatedBy: ".")
        if let integerPart = parts.first {
            return "\(integerPart)" + "°"
        }
        return nil
    }
}
