//
//  NetworkGetRequest.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

typealias SuccessHandler = (_ success: Bool, Error?) -> Void

protocol NetworkGetRequest {
    func getItem<T: Codable>(decodable: T.Type, completion: @escaping (_ items: T?, Error?) -> Void)
    // add more funcs according requirements... for this project we only retrieve once the JSon for the weather
}

extension NetworkGetRequest {

    func getItem<T: Codable>(decodable: T.Type, completion: @escaping (_ items: T?, Error?) -> Void) {
        fatalError("default getItem don't use me")
    }
}

protocol NetworkRequests: NetworkGetRequest { }
