//
//  CoordinatorProtocol.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 21/03/2023.
//

import Foundation
import UIKit

protocol CoordinatorProtocol {
    init(navigationController: UINavigationController)
    var navigationController: UINavigationController { get set }
    func shouldPush(vc: UIViewController, animated: Bool)
    func shouldPop(animated: Bool)
    func startFlow() -> UINavigationController
    func startNewFlow()
}
