//
//  WeatherDataEndpoints.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

enum WeatherDataEndpoints {
    case getWeatherData(lat: String, lon: String, apiKey: String, metric: String)
}

extension WeatherDataEndpoints: Endpoint {

    var baseURL: URL {
        return URL(string: Constants.shared.requestBase)!
    }
    
    var path: String {
        switch self {
        case .getWeatherData:
            return "/data/3.0/onecall?"
        }
    }

    var task: Request {
        switch self {
        case .getWeatherData(let lat, let lon, let apiKey, let metric):
            return .withParams(params: ["lat": lat, "lon": lon, "appid": apiKey, "units": metric])
        }
    }
}

