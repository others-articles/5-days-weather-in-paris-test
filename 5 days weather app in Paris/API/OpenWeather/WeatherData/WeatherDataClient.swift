//
//  WeatherDataClient.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 20/03/2023.
//

import Foundation

protocol WeatherDataClientProtocol: NetworkGetRequest {
    func retrieve<T: Decodable & Encodable>(lat: String, lon: String, apiKey: String, metric: String, decodable: T.Type, completion: @escaping (T?, Error?) -> Void)
}

final class WeatherDataClient {
    private let provider: Provider<WeatherDataEndpoints>

    init(provider: Provider<WeatherDataEndpoints> = Provider<WeatherDataEndpoints>()) {
        self.provider = provider
    }
}

extension WeatherDataClient: WeatherDataClientProtocol {
    func retrieve<T: Decodable & Encodable>(lat: String, lon: String, apiKey: String, metric: String, decodable: T.Type, completion: @escaping (T?, Error?) -> Void) {
        provider.request(.getWeatherData(lat: lat, lon: lon, apiKey: apiKey, metric: metric), decodable: decodable) { result in
            switch result {
            case .success(let response):
                completion(response, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
