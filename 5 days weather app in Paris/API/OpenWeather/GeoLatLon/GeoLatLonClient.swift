//
//  GeoLatLonClient.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 21/03/2023.
//

import Foundation

protocol GeoLatLonClientProtocol: NetworkGetRequest {
    func retrieve<T: Decodable & Encodable>(city: String, limit: String, apiKey: String, decodable: T.Type, completion: @escaping (T?, Error?) -> Void)
}

final class GeoLatLonClient {
    private let provider: Provider<GeoLatLonEndpoints>

    init(provider: Provider<GeoLatLonEndpoints> = Provider<GeoLatLonEndpoints>()) {
        self.provider = provider
    }
}

extension GeoLatLonClient: GeoLatLonClientProtocol {
    func retrieve<T: Decodable & Encodable>(city: String, limit: String, apiKey: String, decodable: T.Type, completion: @escaping (T?, Error?) -> Void) {
        provider.request(.getLatLon(city: city, limit: limit, apiKey: apiKey), decodable: decodable) { result in
            switch result {
            case .success(let response):
                completion(response, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}

