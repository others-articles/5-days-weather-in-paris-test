//
//  GeoLatLonEndpoints.swift
//  5 days weather app in Paris
//
//  Created by Matt Jacquet on 21/03/2023.
//

import Foundation

enum GeoLatLonEndpoints {
    case getLatLon(city: String, limit: String, apiKey: String)
}

extension GeoLatLonEndpoints: Endpoint {

    var baseURL: URL {
        return URL(string: Constants.shared.requestBase)!
    }
    
    var path: String {
        switch self {
        case .getLatLon:
            return "/geo/1.0/direct?"
        }
    }

    var task: Request {
        switch self {
        case .getLatLon(let city, let limit, let apiKey):
            return .withParams(params: ["q": city, "limit": limit, "appid": apiKey])
        }
    }
}

