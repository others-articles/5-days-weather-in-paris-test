# 5 days weather app in Paris - iOS app


# Table of Contents

* Requirements
* Project setup 

##Requirements
* iOS 15.0+
* Xcode 14.2

##Project Setup

Clone the submodule by pasting this command line in the terminal:

``git submodule update — init — recursive
``

Make sure Xcode added the swift packages

