//
//  DayFormatterTests.swift
//  5 days weather app in ParisTests
//
//  Created by Matt Jacquet on 21/03/2023.
//

import XCTest

final class DayFormatterTests: XCTestCase {

    var formatter: DayFormatter!
    
    override func setUp() {
        super.setUp()
        formatter = DayFormatter()
    }
    
    override func tearDown() {
        formatter = nil
        super.tearDown()
    }
    
    func testGetDateFromTimeStamp() {
        let date = formatter.getDateFromTimeStamp(dt: 1616294400) // March 21, 2021 12:00:00 AM UTC
        XCTAssertEqual(date.timeIntervalSince1970, 1616294400)
    }
    
    func testGetNameOfDay() {
        let date = formatter.getDateFromTimeStamp(dt: 1616294400) // March 21, 2021 12:00:00 AM UTC
        let dayName = formatter.getNameOfDay(date: date)
        XCTAssertEqual(dayName, "Sunday")
    }
}
