//
//  TemperatureFormatterTests.swift
//  5 days weather app in ParisTests
//
//  Created by Matt Jacquet on 21/03/2023.
//

import XCTest

class TemperatureFormatterTests: XCTestCase {

    let formatter = TemperatureFormatter()

    func testFormatTemperatureWithValidString() {

        let input = "25.50"
        let expectedOutput = "25°"

        let output = formatter.formatTemperature(temp: input)

        XCTAssertEqual(output, expectedOutput)
    }

    func testFormatTemperatureWithInvalidString() {

        let input = "abc"
        let expectedOutput: String? = nil

        let output = formatter.formatTemperature(temp: input)

        XCTAssertEqual(output, expectedOutput)
    }

    func testFormatTemperatureWithValidDouble() {

        let input = 25.5
        let expectedOutput = "25°"

        let output = formatter.formatTemperature(temp: input)

        XCTAssertEqual(output, expectedOutput)
    }
}
